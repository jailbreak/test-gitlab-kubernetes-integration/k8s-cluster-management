# Kubernetes cluster management

This is a [Cluster Management Project](https://docs.gitlab.com/ee/user/clusters/management_project.html) that contains resources to setup a Kubernetes cluster and configuration for [GitLab Managed Apps](https://docs.gitlab.com/ee/user/clusters/applications.html).

## Setup cluster

To be done once at the beginning.

```bash
kubectl apply -k setup-cluster/
```

## Install apps
